/**
 * Created by victor on 3/16/17.
 */
function Client() {

    const fs = require('fs');

    const servers = {
        ecs: {
            hostname: 'localhost',
            port : 8443
        }
    };

    function makeRequest(json, server, callback) {

        return new Promise((resolve, reject) => {
            const postData = JSON.stringify(json);
            const https = require('https');

            const options = {
                hostname: servers[server].hostname,
                port: servers[server].port,
                path: '/epc',
                method: 'POST',
                ca: fs.readFileSync('./certs/rootCA.crt'),
                headers: {
                    'Content-Type': 'application/json',
                    'Content-Length': Buffer.byteLength(postData)
                },
                cert: fs.readFileSync('./certs/client.crt'),
                key: fs.readFileSync('./certs/client.key'),
                // rejectUnauthorized: false
            };

            let buffer = '';

            const request = https.request(options, function(response) {
                response.on('data', function(data) {
                    buffer += data;
                });

                response.on('end', function(){

                    buffer = JSON.parse(buffer.toString());

                    if (buffer.auth && buffer.auth.crt) {
                        fs.writeFileSync('./certs/client.crt', buffer.auth.crt);
                    }

                    resolve(buffer);

                });
            });

            request.on('error', function(error){
                reject(error);
            });

            request.write(postData);
            request.end();
        });

    }

    function next() {

        let request = {
            "auth": {
                "appid": 12,
                    "compname": "epcentos7x64",
                    "custid": "5a956b7db1a43d79087b23c8",
                    "date": 1519307796,
                    "extraInfo": {
                    "osInfo": {
                        "osKernel": "3.10.0-514.6.1.el7.x86_64",
                            "osMachine": "x86_64",
                            "osPlatformId": 2,
                            "osSubtype": "CentOS Linux",
                            "osType": "Linux",
                            "osVersion": "7.3.1611"
                    }
                },
                "fqds": "epcentos7x64.clearos.lan",
                "hostname": "epcentos7x64",
                "hwid": "564dec81-6d1d-50b0-c69e-e9a7ba3911fc-5254002c7658",
                "ip": "192.168.1.101",
                "isarrakis": false,
                "isvirtual": true,
                "isvirtualserver": true,
                "mac": "000C293911FC",
                "oldhwid": "564DEC81-6D1D-50B0-C69E-E9A7BA3911FC-000C293911FC",
                "os": "CentOS Linux 7.3.1611",
                "rstate": false,
                "shdomainname": "clearos.lan",
                "srvclid": "",
                "service": 3,
                // "csr": fs.readFileSync('./certs/client.csr'),
                // "csrtoken": "Q6HqFZ7sW6RJ"
            }
        };

        // get the initial status
        console.log(new Date(), '[client]', 'request auth status');
        makeRequest(request, 'ecs')
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.error(new Date(), '[client]', 'Error:', error);
            });

    }

    function start() {
        console.log(new Date(), '[client]', 'Starting Client...');

        next();

    }

    return {
        start: start
    };
}

module.exports = Client;
