#!/usr/bin/env bash
openssl genrsa -out ./client.key 2048
openssl req -new -sha256 -key ./client.key -out ./client.csr
