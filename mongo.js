/**
 * Created by victor on 3/16/17.
 */
module.exports = function() {
    let databaseConfig = {
        host : '127.0.0.1',
        port : 27017,
        database : 'serverAuth'
    };

    // get reference to MongoClient and create a new connection
    let client = require("mongodb").MongoClient;

    // build connection url
    let url = `mongodb://${databaseConfig.host}:${databaseConfig.port}/${databaseConfig.database}`;

    return new Promise(function(resolve) {
        // connect to db
        client.connect(url, function (error, database) {

            // connection failed? if so, reject the promise
            if (error) {

                console.log(`Failed to connect to Mongo server ${error}`);
                throw error;

            }

            // connection was successful, fulfill the promise
            // console.log("Connected to Mongo server");
            resolve(database);

        });
    });

};